import cv2
import copy
import onnxruntime
import numpy as np
from typing import Tuple, Optional, List
import torch
from onnx2torch import convert

class YOLOv7ONNX(object):
    def __init__(
        self,
        model_path: Optional[str] = 'yolov7_tiny_head_0.768_post_480x640.onnx',
        model_size: Optional[List] = [480, 640],
        class_score_th: Optional[float] = 0.20
    ):
        """YOLOv7ONNX

        Parameters
        ----------
        model_path: Optional[str]
            ONNX file path for YOLOv7
        class_score_th: Optional[float]
            Score threshold. Default: 0.20
        model_size: Optional[List]
            Model input size [height, width]
            Default: [480, 640]
        """
        # Threshold
        self.class_score_th = class_score_th

        # Model input size
        self.model_size = model_size

        # Model loading
        self.model = convert(model_path)
        self.model.to('cuda' if torch.cuda.is_available() else 'cpu')

    def __call__(
        self,
        image: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """YOLOv7ONNX

        Parameters
        ----------
        image: np.ndarray
            Entire image

        Returns
        -------
        face_boxes: np.ndarray
            Predicted face boxes: [facecount, y1, x1, y2, x2]
        face_scores: np.ndarray
            Predicted face box scores: [facecount, score]
        """
        temp_image = copy.deepcopy(image)

        # PreProcess
        resized_image = self.__preprocess(
            temp_image,
        )

        # Inference
        inferece_image = np.asarray([resized_image], dtype=np.float32)
        with torch.no_grad():
            tensor = torch.from_numpy(inferece_image)
            scores, boxes = self.model(tensor.to("cuda") if torch.cuda.is_available() else tensor)

        if torch.cuda.is_available():
            scores = scores.cpu()
            boxes = boxes.cpu()

        # PostProcess
        face_boxes, face_scores = self.__postprocess(
            image=image,
            scores=scores,
            boxes=boxes
        )

        return face_boxes, face_scores

    def __preprocess(
        self,
        image: np.ndarray,
        swap: Optional[Tuple[int,int,int]] = (2, 0, 1),
    ) -> np.ndarray:
        """__preprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image
        swap: tuple
            HWC to CHW: (2,0,1)
            CHW to HWC: (1,2,0)
            HWC to HWC: (0,1,2)
            CHW to CHW: (0,1,2)

        Returns
        -------
        resized_image: np.ndarray
            Resized and normalized image.
        """
        # Normalization + BGR->RGB
        resized_image = cv2.resize(
            image,
            (
                int(self.model_size[1]),
                int(self.model_size[0]),
            )
        )
        resized_image = np.divide(resized_image, 255.0)
        resized_image = resized_image[..., ::-1]
        resized_image = resized_image.transpose(swap)
        resized_image = np.ascontiguousarray(
            resized_image,
            dtype=np.float32,
        )

        return resized_image

    def __postprocess(
        self,
        image: np.ndarray,
        scores: np.ndarray,
        boxes: np.ndarray
    ) -> Tuple[np.ndarray, np.ndarray]:
        """__postprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image.
        scores: np.ndarray
            float32[N, 1]
        boxes: np.ndarray
            int64[N, 6]

        Returns
        -------
        faceboxes: np.ndarray
            Predicted head boxes: [count, y1, x1, y2, x2]
        facescores: np.ndarray
            Predicted head box confidence: [count, score]
        """
        (image_height, image_width) = image.shape[:2]
        width_scale = image_width / self.model_size[1]
        height_scale = image_height / self.model_size[0]

        """
        Head Detector is
            N -> Number of boxes detected
            batchno -> always 0: BatchNo.0
            classid -> always 0: "Head"
        scores: float32[N,1],
        batchno_classid_y1x1y2x2: int64[N,6],
        """
        scores = scores
        keep_idxs = scores[:, 0] > self.class_score_th
        scores_keep = scores[keep_idxs, :]
        boxes_keep = boxes[keep_idxs, :]
        heads_boxes = []
        heads_scores = []

        if len(boxes_keep) > 0:
            for box, score in zip(boxes_keep, scores_keep):
                x_min = int(box[3] * width_scale)
                y_min = int(box[2] * height_scale)
                x_max = int(box[5] * width_scale)
                y_max = int(box[4] * height_scale)

                x_min = max(x_min, 0)
                y_min = max(y_min, 0)
                x_max = min(x_max, image_width)
                y_max = min(y_max, image_height)

                heads_boxes.append(
                    [x_min, y_min, x_max, y_max]
                )
                heads_scores.append(
                    score
                )

        return np.asarray(heads_boxes), np.asarray(heads_scores)

class DMHeadONNX(object):
    def __init__(
        self,
        model_path: Optional[str] = 'dmhead_mask_Nx3x224x224.onnx',
        model_size: Optional[List] = [224, 224],
        scale_factor: Optional[List] = [10, 5]
    ):
        """DMHeadONNX

        Parameters
        ----------
        mask_model_path: Optional[str]
            ONNX file path for model
        model_size: Optional[List]
            Model input size [height, width]
            Default: [224, 224]
        scale_factor: Optional[stListr]
            Box scale factor [height, width]
            Default: [10, 5]
        """

        # Model input size
        self.model_size = model_size

        self.scale_factor = scale_factor

        # Model loading
        self.model = convert(model_path)
        self.model.to('cuda' if torch.cuda.is_available() else 'cpu')

    def __call__(
        self,
        image: np.ndarray,
        boxes: np.ndarray
    ) -> np.ndarray:
        """DMHeadONNX

        Parameters
        ----------
        image: np.ndarray
            Entire image
        boxes: np.ndarray
            int32[N, 4]

        Returns
        -------
        predicted: np.ndarray
            Predicted yaw, pitch, roll: [count, yaw, pitch, roll]
        """
        
        # PreProcess
        inputs = self.__preprocess(
            image,
            boxes
        )

        # Inference
        with torch.no_grad():
            tensor = torch.from_numpy(inputs)
            outputs = self.model(tensor.to("cuda") if torch.cuda.is_available() else tensor)

        return outputs.cpu().numpy() if torch.cuda.is_available() else outputs.numpy()

    def __preprocess(
        self,
        image: np.ndarray,
        boxes: np.ndarray
    ) -> np.ndarray:
        """__preprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image
        boxes: np.ndarray
            int32[N, 4]

        Returns
        -------
        nchw: np.ndarray
        """
        inputs = []
        (image_height, image_width) = image.shape[:2]

        for box in boxes:
            x_min = int(box[0])
            y_min = int(box[1])
            x_max = int(box[2])
            y_max = int(box[3])

            # enlarge the bbox to include more background margin by scale factor
            y_min = max(0, y_min - abs(y_min - y_max) / self.scale_factor[0])
            y_max = min(image_height, y_max + abs(y_min - y_max) / self.scale_factor[0])
            x_min = max(0, x_min - abs(x_min - x_max) / self.scale_factor[1])
            x_max = min(image_width, x_max + abs(x_min - x_max) / self.scale_factor[1])
            x_max = min(x_max,image_width)
            croped_image = image[int(y_min):int(y_max), int(x_min):int(x_max)]

            # h,w -> 224,224
            croped_resized_image = cv2.resize(croped_image, (self.model_size[1], self.model_size[0]))
            # bgr --> rgb
            rgb = croped_resized_image[..., ::-1]
            # hwc --> chw
            chw = rgb.transpose(2, 0, 1)
            inputs.append(chw)

        # chw --> nchw
        return np.asarray(inputs, dtype=np.float32)