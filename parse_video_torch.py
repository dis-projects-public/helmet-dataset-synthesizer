import os
import cv2
import time
import math
import copy
import argparse
import onnxruntime
import numpy as np
from math import cos, sin
from typing import Tuple, Optional, List
import json
from tqdm import tqdm
import random
import torch
from onnx2torch import convert

class YOLOv7ONNX(object):
    def __init__(
        self,
        model_path: Optional[str] = 'yolov7_tiny_head_0.768_post_480x640.onnx',
        class_score_th: Optional[float] = 0.20,
        model_size: Optional[List] = [480, 640]
    ):
        """YOLOv7ONNX

        Parameters
        ----------
        model_path: Optional[str]
            ONNX file path for YOLOv7
        class_score_th: Optional[float]
            Score threshold. Default: 0.20
        model_size: Optional[List]
            Model input size [height, width]
            Default: [480, 640]
        """
        # Threshold
        self.class_score_th = class_score_th

        # Model input size
        self.model_size = model_size

        # Model loading
        self.model = convert(model_path)
        self.model.to('cuda' if torch.cuda.is_available() else 'cpu')

    def __call__(
        self,
        image: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """YOLOv7ONNX

        Parameters
        ----------
        image: np.ndarray
            Entire image

        Returns
        -------
        face_boxes: np.ndarray
            Predicted face boxes: [facecount, y1, x1, y2, x2]
        face_scores: np.ndarray
            Predicted face box scores: [facecount, score]
        """
        temp_image = copy.deepcopy(image)

        # PreProcess
        resized_image = self.__preprocess(
            temp_image,
        )

        # Inference
        inferece_image = np.asarray([resized_image], dtype=np.float32)
        with torch.no_grad():
            tensor = torch.from_numpy(inferece_image)
            scores, boxes = self.model(tensor.to("cuda") if torch.cuda.is_available() else tensor)

        if torch.cuda.is_available():
            scores = scores.cpu()
            boxes = boxes.cpu()

        # PostProcess
        face_boxes, face_scores = self.__postprocess(
            image=temp_image,
            scores=scores,
            boxes=boxes,
        )

        return face_boxes, face_scores

    def __preprocess(
        self,
        image: np.ndarray,
        swap: Optional[Tuple[int,int,int]] = (2, 0, 1),
    ) -> np.ndarray:
        """__preprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image
        swap: tuple
            HWC to CHW: (2,0,1)
            CHW to HWC: (1,2,0)
            HWC to HWC: (0,1,2)
            CHW to CHW: (0,1,2)

        Returns
        -------
        resized_image: np.ndarray
            Resized and normalized image.
        """
        # Normalization + BGR->RGB
        resized_image = cv2.resize(
            image,
            (
                int(self.model_size[1]),
                int(self.model_size[0]),
            )
        )
        resized_image = np.divide(resized_image, 255.0)
        resized_image = resized_image[..., ::-1]
        resized_image = resized_image.transpose(swap)
        resized_image = np.ascontiguousarray(
            resized_image,
            dtype=np.float32,
        )
        return resized_image

    def __postprocess(
        self,
        image: np.ndarray,
        scores: np.ndarray,
        boxes: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """__postprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image.
        scores: np.ndarray
            float32[N, 1]
        boxes: np.ndarray
            int64[N, 6]

        Returns
        -------
        faceboxes: np.ndarray
            Predicted face boxes: [facecount, y1, x1, y2, x2]
        facescores: np.ndarray
            Predicted face box confs: [facecount, score]
        """
        image_height = image.shape[0]
        image_width = image.shape[1]

        """
        Head Detector is
            N -> Number of boxes detected
            batchno -> always 0: BatchNo.0
            classid -> always 0: "Head"
        scores: float32[N,1],
        batchno_classid_y1x1y2x2: int64[N,6],
        """
        scores = scores
        keep_idxs = scores[:, 0] > self.class_score_th
        scores_keep = scores[keep_idxs, :]
        boxes_keep = boxes[keep_idxs, :]
        faceboxes = []
        facescores = []

        if len(boxes_keep) > 0:
            for box, score in zip(boxes_keep, scores_keep):
                x_min = max(int(box[3]), 0)
                y_min = max(int(box[2]), 0)
                x_max = min(int(box[5]), image_width)
                y_max = min(int(box[4]), image_height)

                faceboxes.append(
                    [x_min, y_min, x_max, y_max]
                )
                facescores.append(
                    score
                )

        return np.asarray(faceboxes), np.asarray(facescores)



def draw_axis(img, yaw, pitch, roll, tdx=None, tdy=None, size=100):
    # Referenced from HopeNet https://github.com/natanielruiz/deep-head-pose
    if math.isnan(yaw) or math.isnan(pitch) or math.isnan(roll):
        return img
    pitch = pitch * np.pi / 180
    yaw = -(yaw * np.pi / 180)
    roll = roll * np.pi / 180
    if tdx != None and tdy != None:
        tdx = tdx
        tdy = tdy
    else:
        height, width = img.shape[:2]
        tdx = width / 2
        tdy = height / 2
    # X-Axis pointing to right. drawn in red
    x1 = size * (cos(yaw) * cos(roll)) + tdx
    y1 = size * (cos(pitch) * sin(roll) + cos(roll) * sin(pitch) * sin(yaw)) + tdy
    # Y-Axis | drawn in green
    #        v
    x2 = size * (-cos(yaw) * sin(roll)) + tdx
    y2 = size * (cos(pitch) * cos(roll) - sin(pitch) * sin(yaw) * sin(roll)) + tdy
    # Z-Axis (out of the screen) drawn in blue
    x3 = size * (sin(yaw)) + tdx
    y3 = size * (-cos(yaw) * sin(pitch)) + tdy
    cv2.line(img, (int(tdx), int(tdy)), (int(x1),int(y1)),(0,0,255),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x2),int(y2)),(0,255,0),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x3),int(y3)),(255,0,0),2)
    return img


def main(args):
    # YOLOv7_tiny_Head
    yolo_H = 480 # Yolo model input height
    yolo_W = 640 # Yolo model input width

    yolov7_head = YOLOv7ONNX(
        class_score_th=0.20,
    )

    # DMHead
    model_file_path = ''
    mask_or_nomask = args.mask_or_nomask
    dmhead_H = 224 # DMHead model input height
    dmhead_W = 224 # DMHead model input width

    if mask_or_nomask == 'mask':
        model_file_path = 'dmhead_mask_Nx3x224x224.onnx'
    elif mask_or_nomask == 'nomask':
        model_file_path = 'dmhead_nomask_Nx3x224x224.onnx'

    dmhead = convert(model_file_path)
    dmhead.to('cuda' if torch.cuda.is_available() else 'cpu')

    #cap_width = int(args.height_width.split('x')[1])
    #cap_height = int(args.height_width.split('x')[0])

    input_file = os.path.basename(args.input)
    input_name, input_extension = os.path.splitext(input_file)

    output_file = f"{args.output}/{input_name}" if args.output else input_name
    output_video = f"{output_file}_heads.mp4"
    output_json = f"{output_file}.json"

    cap = cv2.VideoCapture(args.input)
    cap_fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video_writer = cv2.VideoWriter(
        filename=output_video,
        fourcc=fourcc,
        fps=cap_fps,
        frameSize=(w, h),
    )

    frame_index = 0
    output_data = []

    print('Input:', args.input)

    #frames_list = random.sample(range(0, frame_count-1), int(args.frames)) if args.frames else None

    if not args.debug:
        pbar = tqdm(total=args.frames if args.frames else int(frame_count))

    while True:
        #if frames_list:
        #    cap.set(cv2.CAP_PROP_POS_FRAMES, frames_list[frame_index])

        ret, frame = cap.read()
        if not ret:
            break

        start = time.time()

        # ============================================================= YOLOv7_tiny_Head
        heads, head_scores = yolov7_head(frame)

        canvas = copy.deepcopy(frame)
        # ============================================================= DMHead
        croped_resized_frame = None

        # Scale factors for resize bbox to output video height and width
        h_factor = h / yolo_H
        w_factor = w / yolo_W

        if len(heads) > 0:
            dmhead_inputs = []
            dmhead_position = []

            for head in heads:
                #x_min = int(head[0])
                #y_min = int(head[1])
                #x_max = int(head[2])
                #y_max = int(head[3])

                x_min = int(head[0] * w_factor)
                y_min = int(head[1] * h_factor)
                x_max = int(head[2] * w_factor)
                y_max = int(head[3] * h_factor)
                dmhead_position.append([x_min,y_min,x_max,y_max])

                # enlarge the bbox to include more background margin
                y_min = max(0, y_min - abs(y_min - y_max) / 10)
                y_max = min(frame.shape[0], y_max + abs(y_min - y_max) / 10)
                x_min = max(0, x_min - abs(x_min - x_max) / 5)
                x_max = min(frame.shape[1], x_max + abs(x_min - x_max) / 5)
                x_max = min(x_max, frame.shape[1])
                croped_frame = frame[int(y_min):int(y_max), int(x_min):int(x_max)]

                # h,w -> 224,224
                croped_resized_frame = cv2.resize(croped_frame, (dmhead_W, dmhead_H))
                # bgr --> rgb
                rgb = croped_resized_frame[..., ::-1]
                # hwc --> chw
                chw = rgb.transpose(2, 0, 1)
                dmhead_inputs.append(chw)
                #dmhead_position.append([x_min,y_min,x_max,y_max])
            # chw --> nchw
            nchw = np.asarray(dmhead_inputs, dtype=np.float32)
            positions = np.asarray(dmhead_position, dtype=np.int32)

            yaw = 0.0
            pitch = 0.0
            roll = 0.0
            # Inference DMHead
            with torch.no_grad():
                tensor = torch.from_numpy(nchw)
                outputs = dmhead(tensor.to("cuda") if torch.cuda.is_available() else tensor)

            outputs = outputs.cpu().numpy() if torch.cuda.is_available() else outputs.numpy()

            if outputs.size > 0:
                heads_data = []

                for (yaw, roll, pitch), position in zip(outputs, positions):
                    yaw, pitch, roll = np.squeeze([yaw, pitch, roll])
                    if args.debug:
                        print(f'yaw: {yaw}, pitch: {pitch}, roll: {roll}')

                    x_min,y_min,x_max,y_max = position

                    # Heads data
                    heads_data.append({
                        "x_min": int(x_min),
                        "y_min": int(y_min),
                        "x_max": int(x_max),
                        "y_max": int(y_max),
                        "yaw": int(yaw),
                        "pitch": int(pitch),
                        "roll": int(roll)
                    })

                    # BBox draw
                    deg_norm = 1.0 - abs(yaw / 180)
                    blue = int(255 * deg_norm)
                    cv2.rectangle(
                        canvas,
                        (int(x_min), int(y_min)),
                        (int(x_max), int(y_max)),
                        color=(blue, 0, 255-blue),
                        thickness=2
                    )

                    # Draw
                    draw_axis(
                        canvas,
                        yaw,
                        pitch,
                        roll,
                        tdx=(x_min+x_max)/2,
                        tdy=(y_min+y_max)/2,
                        size=abs(x_max-x_min)//2
                    )
                    cv2.putText(
                        canvas,
                        f'yaw: {np.round(yaw)}',
                        (int(x_min), int(y_min)),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.4,
                        (100, 255, 0),
                        1
                    )
                    cv2.putText(
                        canvas,
                        f'pitch: {np.round(pitch)}',
                        (int(x_min), int(y_min) - 15),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.4,
                        (100, 255, 0),
                        1
                    )
                    cv2.putText(
                        canvas,
                        f'roll: {np.round(roll)}',
                        (int(x_min), int(y_min)-30),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.4,
                        (100, 255, 0),
                        1
                    )

                output_data.append({"frame": frame_index, "heads": heads_data})

        time_txt = f'{(time.time()-start)*1000:.2f} ms (inference+post-process)'
        cv2.putText(
            canvas,
            time_txt,
            (20, 35),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.8,
            (255, 255, 255),
            2,
            cv2.LINE_AA,
        )
        cv2.putText(
            canvas,
            time_txt,
            (20, 35),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.8,
            (0, 255, 0),
            1,
            cv2.LINE_AA,
        )

        video_writer.write(canvas)

        if args.debug:
            print('Frame index:', frame_index)
        else:            
            pbar.update(1)

        frame_index += 1

        if args.frames:
            if frame_index == args.frames:
                break
    
    if not args.debug:
        pbar.close()

    json_object = json.dumps(output_data, indent=4)
    with open(output_json, "w") as outfile:
        outfile.write(json_object)

    print('Frames with heads:', len(output_data))

    if video_writer:
        video_writer.release()

    if cap:
        cap.release()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        type=str,
        default='',
        help='Path of the mp4 input file.',
    )
    parser.add_argument(
        '--output',
        type=str,
        default='',
        help='Path of the mp4 and json output file.',
    )
    parser.add_argument(
        '--mask_or_nomask',
        type=str,
        default='mask',
        choices=[
            'mask',
            'nomask',
        ],
        help='\
            Select either a model that provides high accuracy when wearing \
            a mask or a model that provides high accuracy when not wearing a mask.',
    )
    parser.add_argument(
        '--frames',
        type=int,
        default=None,
        help='Count of random frames to parse.'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Show debug information. Default: False.'
    )
    args = parser.parse_args()
    main(args)
