import cv2
import time
import math
import copy
import argparse
import numpy as np
from math import cos, sin
from typing import Tuple, Optional, List
import json
from tqdm import tqdm
import torch
from onnx2torch import convert
import random
import matplotlib.pyplot as plt
import glob
import os
from enum import Enum

class YOLOv7ONNX(object):
    def __init__(
        self,
        model_path: Optional[str] = 'yolov7_tiny_head_0.768_post_480x640.onnx',
        class_score_th: Optional[float] = 0.20,
        model_size: Optional[List] = [480, 640]
    ):
        """YOLOv7ONNX

        Parameters
        ----------
        model_path: Optional[str]
            ONNX file path for YOLOv7
        class_score_th: Optional[float]
            Score threshold. Default: 0.20
        model_size: Optional[List]
            Model input size [height, width]
            Default: [480, 640]
        """
        # Threshold
        self.class_score_th = class_score_th

        # Model input size
        self.model_size = model_size

        # Model loading
        self.model = convert(model_path)
        self.model.to('cuda' if torch.cuda.is_available() else 'cpu')

    def __call__(
        self,
        image: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """YOLOv7ONNX

        Parameters
        ----------
        image: np.ndarray
            Entire image

        Returns
        -------
        face_boxes: np.ndarray
            Predicted face boxes: [facecount, y1, x1, y2, x2]
        face_scores: np.ndarray
            Predicted face box scores: [facecount, score]
        """
        temp_image = copy.deepcopy(image)

        # PreProcess
        resized_image = self.__preprocess(
            temp_image,
        )

        # Inference
        inferece_image = np.asarray([resized_image], dtype=np.float32)
        with torch.no_grad():
            tensor = torch.from_numpy(inferece_image)
            scores, boxes = self.model(tensor.to("cuda") if torch.cuda.is_available() else tensor)

        if torch.cuda.is_available():
            scores = scores.cpu()
            boxes = boxes.cpu()

        # PostProcess
        face_boxes, face_scores = self.__postprocess(
            image=temp_image,
            scores=scores,
            boxes=boxes,
        )

        return face_boxes, face_scores

    def __preprocess(
        self,
        image: np.ndarray,
        swap: Optional[Tuple[int,int,int]] = (2, 0, 1),
    ) -> np.ndarray:
        """__preprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image
        swap: tuple
            HWC to CHW: (2,0,1)
            CHW to HWC: (1,2,0)
            HWC to HWC: (0,1,2)
            CHW to CHW: (0,1,2)

        Returns
        -------
        resized_image: np.ndarray
            Resized and normalized image.
        """
        # Normalization + BGR->RGB
        resized_image = cv2.resize(
            image,
            (
                int(self.model_size[1]),
                int(self.model_size[0]),
            )
        )
        resized_image = np.divide(resized_image, 255.0)
        resized_image = resized_image[..., ::-1]
        resized_image = resized_image.transpose(swap)
        resized_image = np.ascontiguousarray(
            resized_image,
            dtype=np.float32,
        )
        return resized_image

    def __postprocess(
        self,
        image: np.ndarray,
        scores: np.ndarray,
        boxes: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """__postprocess

        Parameters
        ----------
        image: np.ndarray
            Entire image.
        scores: np.ndarray
            float32[N, 1]
        boxes: np.ndarray
            int64[N, 6]

        Returns
        -------
        faceboxes: np.ndarray
            Predicted face boxes: [facecount, y1, x1, y2, x2]
        facescores: np.ndarray
            Predicted face box confs: [facecount, score]
        """
        image_height = image.shape[0]
        image_width = image.shape[1]

        """
        Head Detector is
            N -> Number of boxes detected
            batchno -> always 0: BatchNo.0
            classid -> always 0: "Head"
        scores: float32[N,1],
        batchno_classid_y1x1y2x2: int64[N,6],
        """
        scores = scores
        keep_idxs = scores[:, 0] > self.class_score_th
        scores_keep = scores[keep_idxs, :]
        boxes_keep = boxes[keep_idxs, :]
        faceboxes = []
        facescores = []

        if len(boxes_keep) > 0:
            for box, score in zip(boxes_keep, scores_keep):


                x_min = max(int(box[3]), 0)
                y_min = max(int(box[2]), 0)
                x_max = min(int(box[5]), image_width)
                y_max = min(int(box[4]), image_height)

                faceboxes.append(
                    [x_min, y_min, x_max, y_max]
                )
                facescores.append(
                    score
                )

        return np.asarray(faceboxes), np.asarray(facescores)


class YAW(Enum):
    FRONT = 1
    FRONT_LEFT = 2
    FRONT_RIGHT = 3
    REAR = 4
    REAR_LEFT = 5
    REAR_RIGHT = 6
    LEFT = 7
    RIGHT = 8

    @classmethod
    def from_value(cls, value):
        if value >= 0:
            if value >= 150:
                return cls.REAR
            elif value >= 120:
                return cls.REAR_LEFT
            elif value >= 60:
                return cls.LEFT
            elif value >= 30:
                return cls.FRONT_LEFT
        else:
            if value <= -150:
                return cls.REAR
            elif value <= -120:
                return cls.REAR_RIGHT
            elif value <= -60:
                return cls.RIGHT
            elif value <= -30:
                return cls.FRONT_RIGHT

        return cls.FRONT

class HEAD(object):
    def __init__(self, data, scale: Optional[float]=None):
        self.o_ymin = int(data["y_min"])
        self.o_ymax = int(data["y_max"])
        self.o_xmax = int(data["x_max"])
        self.o_xmin = int(data["x_min"])
        self.roll = data["roll"]
        self.pitch = data["pitch"]
        self.yaw = YAW.from_value(data["yaw"])
        self.category = 0

        self.ymin = self.o_ymin
        self.ymax = self.o_ymax
        self.xmin = self.o_xmin
        self.xmax = self.o_xmax

        if scale:
            sf = scale / 100
            # Height
            self.ymin -= int(sf * (self.o_ymax - self.o_ymin))
            self.ymax += int(sf * (self.o_ymax - self.o_ymin))
            # Width
            self.xmin -= int(sf * (self.o_xmax - self.o_xmin))
            self.xmax += int(sf * (self.o_xmax - self.o_xmin))

        self.height = int(self.ymax - self.ymin)
        self.width = int(self.xmax - self.xmin)

    def overlap(self, head, scaled=True):
        if scaled:
            if self.xmin > head.xmax or head.xmin > self.xmax:
                return False
        
            # If one rectangle is above other
            if self.ymin > head.ymax or head.ymin > self.ymax:
                return False
        else:
            if self.o_xmin > head.o_xmax or head.o_xmin > self.o_xmax:
                return False
        
            # If one rectangle is above other
            if self.o_ymin > head.o_ymax or head.o_ymin > self.o_ymax:
                return False
    
        return True

    def area(self, scaled=True):
        if scaled:
            return (self.xmax - self.xmin) * (self.ymax - self.ymin)
        else:
            return (self.o_xmax - self.o_xmin) * (self.o_ymax - self.o_ymin)

    def bbox(self, percent, scaled=True):
        sf = percent / 100

        if scaled:
            # Height
            ymin = self.ymin - int(sf * (self.ymax - self.ymin))
            ymax = self.ymax + int(sf * (self.ymax - self.ymin))
            # Width
            xmin = self.xmin - int(sf * (self.xmax - self.xmin))
            xmax = self.xmax + int(sf * (self.xmax - self.xmin))
        else:
            # Height
            ymin = self.o_ymin - int(sf * (self.o_ymax - self.o_ymin))
            ymax = self.o_ymax + int(sf * (self.o_ymax - self.o_ymin))
            # Width
            xmin = self.o_xmin - int(sf * (self.o_xmax - self.o_xmin))
            xmax = self.o_xmax + int(sf * (self.o_xmax - self.o_xmin))

        return ymin, ymax, xmin, xmax

    def coco(self, percent, scaled=True):
        sf = percent / 100

        if scaled:
            # Height
            ymin = self.ymin - int(sf * (self.ymax - self.ymin))
            ymax = self.ymax + int(sf * (self.ymax - self.ymin))
            # Width
            xmin = self.xmin - int(sf * (self.xmax - self.xmin))
            xmax = self.xmax + int(sf * (self.xmax - self.xmin))
        else:
            # Height
            ymin = self.o_ymin - int(sf * (self.o_ymax - self.o_ymin))
            ymax = self.o_ymax + int(sf * (self.o_ymax - self.o_ymin))
            # Width
            xmin = self.o_xmin - int(sf * (self.o_xmax - self.o_xmin))
            xmax = self.o_xmax + int(sf * (self.o_xmax - self.o_xmin))

        return [xmin, ymin, xmax - xmin, ymax - ymin]

def draw_axis(img, yaw, pitch, roll, tdx=None, tdy=None, size=100):
    # Referenced from HopeNet https://github.com/natanielruiz/deep-head-pose
    if math.isnan(yaw) or math.isnan(pitch) or math.isnan(roll):
        return img
    pitch = pitch * np.pi / 180
    yaw = -(yaw * np.pi / 180)
    roll = roll * np.pi / 180
    if tdx != None and tdy != None:
        tdx = tdx
        tdy = tdy
    else:
        height, width = img.shape[:2]
        tdx = width / 2
        tdy = height / 2
    # X-Axis pointing to right. drawn in red
    x1 = size * (cos(yaw) * cos(roll)) + tdx
    y1 = size * (cos(pitch) * sin(roll) + cos(roll) * sin(pitch) * sin(yaw)) + tdy
    # Y-Axis | drawn in green
    #        v
    x2 = size * (-cos(yaw) * sin(roll)) + tdx
    y2 = size * (cos(pitch) * cos(roll) - sin(pitch) * sin(yaw) * sin(roll)) + tdy
    # Z-Axis (out of the screen) drawn in blue
    x3 = size * (sin(yaw)) + tdx
    y3 = size * (-cos(yaw) * sin(pitch)) + tdy
    cv2.line(img, (int(tdx), int(tdy)), (int(x1),int(y1)),(0,0,255),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x2),int(y2)),(0,255,0),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x3),int(y3)),(255,0,0),2)
    return img

def parse_heads(heads, scale=None, min_size=None, max_size=None, overlap=10):
    helmets = []
    no_helmets = []
    overlap_scale = 1 + (overlap / 100)
    
    for a in heads:
        success = True
        head1 = HEAD(a, scale)

        for b in heads:
            if a != b:
                head2 = HEAD(b, scale)
                if head1.overlap(head2):
                    if head1.area() < head2.area() * overlap_scale:
                        success = False
                        break

                if min_size:
                    if (head1.s_xmax - head1.s_xmin) < min_size:
                        success = False
                        break

                    if (head1.s_ymax - head1.s_ymin) < min_size:
                        success = False
                        break

                if max_size:
                    if (head1.s_xmax - head1.s_xmin) > max_size:
                        success = False
                        break

                    if (head1.s_ymax - head1.s_ymin) > max_size:
                        success = False
                        break

        if success:
            helmets.append(head1)
        else:
            no_helmets.append(head1)

    return helmets, no_helmets

def list_dirs(path):
    directories = [x[1] for x in os.walk(path)]
    non_empty_dirs = [x for x in directories if x]
    return [item for subitem in non_empty_dirs for item in subitem]

def get_helmets_dir(yaw, dirs):
    if yaw == YAW.REAR:
        return "rear", False if "rear" in dirs else (None, False)
    elif yaw == YAW.REAR_LEFT:
        return ("rear_left", False) if "rear_left" in dirs else ("rear_right", True) if "rear_right" in dirs else (None, False)
    elif yaw == YAW.LEFT:
        return ("left", False) if "left" in dirs else ("right", True) if "right" in dirs else (None, False)
    elif yaw == YAW.FRONT_LEFT:
        return ("front_left", False) if "front_left" in dirs else ("front_right", True) if "front_right" in dirs else (None, False)
    elif yaw == YAW.REAR_RIGHT:
        return ("rear_right", False) if "rear_right" in dirs else ("rear_left", True) if "rear_left" in dirs else (None, False)
    elif yaw == YAW.RIGHT:
        return ("right", False) if "right" in dirs else ("left", True) if "left" in dirs else (None, False)
    elif yaw == YAW.FRONT_RIGHT:
        return ("front_right", False) if "front_right" in dirs else ("front_left", True) if "front_left" in dirs else (None, False)

    return "front", False if "front" in dirs else (None, False)

def get_helmet_image(head, path, helmets):
    helmet_dir, flip = get_helmets_dir(head.yaw, helmets)

    if helmet_dir:
        images = glob.glob(path + '/' + helmet_dir + '/*.png')

        if len(images):
            index = random.randint(0, len(images)-1)
            image = cv2.imread(images[index], cv2.IMREAD_UNCHANGED)

            if flip:
                image = cv2.flip(image, 1)

            (h, w) = image.shape[:2]
            if w > h:
                r = head.width / float(w)
                dim = (head.width, int(h * r))
            else:
                r = head.height / float(h)
                dim = (int(w * r), head.height)

            return cv2.resize(image, dim, interpolation = cv2.INTER_AREA), True
    
    return None, False

def add_helmet_image(background, background_mask, head, helmet):
    bg_h, bg_w, bg_channels = background.shape
    fg_h, fg_w, fg_channels = helmet.shape

    assert bg_channels == 3, f'background image should have exactly 3 channels (RGB). found:{bg_channels}'
    assert fg_channels == 4, f'foreground image should have exactly 4 channels (RGBA). found:{fg_channels}'

    # Calcule offset
    (fh, fw) = helmet.shape[:2]

    x_center = int((head.xmax - head.xmin) / 2) + head.xmin
    y_center = int((head.ymax - head.ymin) / 2) + head.ymin
    x_offset = x_center - int(fw / 2)
    y_offset = y_center - int(fh / 2)

    w = min(fg_w, bg_w, fg_w + x_offset, bg_w - x_offset)
    h = min(fg_h, bg_h, fg_h + y_offset, bg_h - y_offset)

    if w < 1 or h < 1: return

    # clip foreground and background images to the overlapping regions
    bg_x = max(0, x_offset)
    bg_y = max(0, y_offset)
    fg_x = max(0, x_offset * -1)
    fg_y = max(0, y_offset * -1)
    helmet = helmet[fg_y:fg_y + h, fg_x:fg_x + w]
    background_subsection = background[bg_y:bg_y + h, bg_x:bg_x + w]

    # separate alpha and color channels from the foreground image
    foreground_colors = helmet[:, :, :3]
    alpha_channel = helmet[:, :, 3] / 255  # 0-255 => 0.0-1.0

    # construct an alpha_mask that matches the image shape
    alpha_mask = np.dstack((alpha_channel, alpha_channel, alpha_channel))

    # combine the background with the overlay image weighted by alpha
    composite = background_subsection * (1 - alpha_mask) + foreground_colors * alpha_mask

    # overwrite the section of the background image that has been updated
    background[bg_y:bg_y + h, bg_x:bg_x + w] = composite

    mask_subsection = background_mask[bg_y:bg_y + h, bg_x:bg_x + w]
    mask = np.zeros(helmet.shape, dtype=np.uint8)
    mask.fill(255)
    background_mask[bg_y:bg_y + h, bg_x:bg_x + w] = mask_subsection * (1 - alpha_mask) + mask[:, :, :3] * alpha_mask

    """
    plt.imshow(background_mask)
    plt.axis("off")
    plt.show()
    """

    return y_offset, y_offset + h, x_offset, x_offset + w

def coco2yolo(height, width, bbox):
    """
    Convert bounding box from COCO  format to YOLO format

    Parameters
    ----------
    img_width : int
        width of image
    img_height : int
        height of image
    bbox : list[int]
        bounding box annotation in COCO format: 
        [top left x position, top left y position, width, height]

    Returns
    -------
    list[float]
        bounding box annotation in YOLO format: 
        [x_center_rel, y_center_rel, width_rel, height_rel]
    """
    
    # YOLO bounding box format: [x_center, y_center, width, height]
    # (float values relative to width and height of image)
    x_tl, y_tl, w, h = bbox

    dw = 1.0 / width
    dh = 1.0 / height

    x_center = x_tl + w / 2.0
    y_center = y_tl + h / 2.0

    x = x_center * dw
    y = y_center * dh
    w = w * dw
    h = h * dh

    return [x, y, w, h]

"""
def make_folders(path="output"):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)
    return path

#path = make_folders(output_path)
"""

def save_yolo_txt(filename, height, width, data):
    with open(filename, "w") as f:
        for item in data:
            bbox = item["bbox"]
            category = item["category"]
            x, y, w, h = coco2yolo(height, width, bbox)
            f.write(f"{category} {x:.6f} {y:.6f} {w:.6f} {h:.6f}\n")

    print("YOLO txt saved!")

def main(args):
    # YOLOv7_tiny_Head
    yolo_H = 480 # Yolo model input height
    yolo_W = 640 # Yolo model input width

    yolov7_head = YOLOv7ONNX(
        class_score_th=0.20,
    )

    # DMHead
    model_file_path = ''
    mask_or_nomask = args.mask_or_nomask
    dmhead_H = 224 # DMHead model input height
    dmhead_W = 224 # DMHead model input width

    if mask_or_nomask == 'mask':
        model_file_path = 'dmhead_mask_Nx3x224x224.onnx'
    elif mask_or_nomask == 'nomask':
        model_file_path = 'dmhead_nomask_Nx3x224x224.onnx'

    dmhead = convert(model_file_path)
    dmhead.to('cuda' if torch.cuda.is_available() else 'cpu')

    #cap_width = int(args.height_width.split('x')[1])
    #cap_height = int(args.height_width.split('x')[0])

    print('Input:', args.input)

    heads_cache = args.cache == "True"
    debug_info = args.debug == "True"

    input_file = os.path.basename(args.input)
    input_name, input_extension = os.path.splitext(input_file)
    output_json = input_name + ".json"

    cap = cv2.VideoCapture(args.input)
    cap_fps = cap.get(cv2.CAP_PROP_FPS)
    cap_frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    frames_count = int(args.frames) if args.frames else int(cap_frame_count)

    output_heads = None
    
    if heads_cache and os.path.isfile(output_json):
        with open(output_json, 'r') as openfile:
            output_heads = json.load(openfile)

    if output_heads:
        print("Estimated heads position from cached file:", output_json)
    else:
        output_heads = []

        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(
            filename=input_name + "_heads.mp4",
            fourcc=fourcc,
            fps=cap_fps,
            frameSize=(w, h),
        )

        frame_index = 0

        print("Processing video: Estimating heads position")
        pbar = tqdm(total=frames_count)

        while True:
            ret, frame = cap.read()
            if not ret:
                break

            start = time.time()

            # ============================================================= YOLOv7_tiny_Head
            heads, head_scores = yolov7_head(frame)

            canvas = copy.deepcopy(frame)
            # ============================================================= DMHead
            croped_resized_frame = None

            # Scale factors for resize bbox to output video height and width
            h_factor = h / yolo_H
            w_factor = w / yolo_W

            if len(heads) > 0:
                dmhead_inputs = []
                dmhead_position = []

                for head in heads:
                    #x_min = int(head[0])
                    #y_min = int(head[1])
                    #x_max = int(head[2])
                    #y_max = int(head[3])

                    x_min = int(head[0] * w_factor)
                    y_min = int(head[1] * h_factor)
                    x_max = int(head[2] * w_factor)
                    y_max = int(head[3] * h_factor)
                    dmhead_position.append([x_min,y_min,x_max,y_max])

                    # enlarge the bbox to include more background margin
                    y_min = max(0, y_min - abs(y_min - y_max) / 10)
                    y_max = min(frame.shape[0], y_max + abs(y_min - y_max) / 10)
                    x_min = max(0, x_min - abs(x_min - x_max) / 5)
                    x_max = min(frame.shape[1], x_max + abs(x_min - x_max) / 5)
                    x_max = min(x_max, frame.shape[1])
                    croped_frame = frame[int(y_min):int(y_max), int(x_min):int(x_max)]

                    # h,w -> 224,224
                    croped_resized_frame = cv2.resize(croped_frame, (dmhead_W, dmhead_H))
                    # bgr --> rgb
                    rgb = croped_resized_frame[..., ::-1]
                    # hwc --> chw
                    chw = rgb.transpose(2, 0, 1)
                    dmhead_inputs.append(chw)
                    #dmhead_position.append([x_min,y_min,x_max,y_max])
                # chw --> nchw
                nchw = np.asarray(dmhead_inputs, dtype=np.float32)
                positions = np.asarray(dmhead_position, dtype=np.int32)

                yaw = 0.0
                pitch = 0.0
                roll = 0.0
                # Inference DMHead
                with torch.no_grad():
                    tensor = torch.from_numpy(nchw)
                    outputs = dmhead(tensor.to("cuda") if torch.cuda.is_available() else tensor)

                outputs = outputs.cpu().numpy() if torch.cuda.is_available() else outputs.numpy()

                if outputs.size > 0:
                    heads_data = []

                    for (yaw, roll, pitch), position in zip(outputs, positions):
                        yaw, pitch, roll = np.squeeze([yaw, pitch, roll])
                        #if debug_info:
                        #    print(f'yaw: {yaw}, pitch: {pitch}, roll: {roll}')

                        x_min,y_min,x_max,y_max = position

                        # Heads data
                        heads_data.append({
                            "x_min": int(x_min),
                            "y_min": int(y_min),
                            "x_max": int(x_max),
                            "y_max": int(y_max),
                            "yaw": int(yaw),
                            "pitch": int(pitch),
                            "roll": int(roll)
                        })

                        # BBox draw
                        deg_norm = 1.0 - abs(yaw / 180)
                        blue = int(255 * deg_norm)
                        cv2.rectangle(
                            canvas,
                            (int(x_min), int(y_min)),
                            (int(x_max), int(y_max)),
                            color=(blue, 0, 255-blue),
                            thickness=2
                        )

                        # Draw
                        draw_axis(
                            canvas,
                            yaw,
                            pitch,
                            roll,
                            tdx=(x_min+x_max)/2,
                            tdy=(y_min+y_max)/2,
                            size=abs(x_max-x_min)//2
                        )
                        cv2.putText(
                            canvas,
                            f'yaw: {np.round(yaw)}',
                            (int(x_min), int(y_min)),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.4,
                            (100, 255, 0),
                            1
                        )
                        cv2.putText(
                            canvas,
                            f'pitch: {np.round(pitch)}',
                            (int(x_min), int(y_min) - 15),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.4,
                            (100, 255, 0),
                            1
                        )
                        cv2.putText(
                            canvas,
                            f'roll: {np.round(roll)}',
                            (int(x_min), int(y_min)-30),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.4,
                            (100, 255, 0),
                            1
                        )

                    output_heads.append({"frame": frame_index, "heads": heads_data})

            time_txt = f'{(time.time()-start)*1000:.2f} ms (inference+post-process)'
            cv2.putText(
                canvas,
                time_txt,
                (20, 35),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.8,
                (255, 255, 255),
                2,
                cv2.LINE_AA,
            )
            cv2.putText(
                canvas,
                time_txt,
                (20, 35),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.8,
                (0, 255, 0),
                1,
                cv2.LINE_AA,
            )

            video_writer.write(canvas)
      
            pbar.update(1)
            frame_index += 1

            if args.frames:
                if frame_index == args.frames:
                    break
        
        pbar.close()

        json_object = json.dumps(output_heads, indent=4)
        with open(output_json, "w") as outfile:
            outfile.write(json_object)

        print('Frames with estimated heads position:', len(output_heads))

        if video_writer:
            video_writer.release()

    data_length = len(output_heads)

    if data_length > 0:
        timestamp = int(time.time())

        helmets_dirs = list_dirs(args.helmets)

        if args.output:
            if not os.path.exists(args.output):
                print("Making output directory:", args.output)
                os.makedirs(args.output)

        output_path = args.output + "/" if args.output else ""

        frames_list = range(frames_count)

        if args.count:
            frames_list = random.sample(range(frames_count), int(args.count)) if args.count.isdecimal() else [int(value) for value in args.count.split(',')]

        if debug_info:
            print("Synthesize frames:", "All" if len(frames_list) == data_length else frames_list)

        data_index = 0

        while data_index < data_length:
            frame_data = output_heads[data_index]
            frame_index = frame_data["frame"]
            
            if not frame_index in frames_list:
                data_index += 1
                continue

            cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
            ret, frame = cap.read()
            if not ret:
                break

            if debug_info:
                print(frame_data)

            heads = frame_data["heads"]

            try_helmets, no_helmets = parse_heads(heads, args.scale, args.min_size, args.max_size)
            helmets = []

            count = int(len(heads) * (args.percent/100))
            helmets_count = len(try_helmets)
            print(count, helmets_count)
            synthesize = list(range(helmets_count)) if count >= helmets_count else random.sample(range(helmets_count), count)

            if debug_info:
                print("Synthesize heads:", synthesize)

            frame_mask = frame.copy()
            frame_mask[:] = (0, 0, 0)
            
            helmet_index = 0
            for head in try_helmets:
                if helmet_index in synthesize:
                    helmet_image, success = get_helmet_image(head, args.helmets, helmets_dirs)
                    if success:
                        head.ymin, head.ymax, head.xmin, head.xmax = add_helmet_image(frame, frame_mask, head, helmet_image)
                        head.category = 1
                        helmets.append(head)
                    else:
                        print("Can't get image for helmet:", head.yaw)
                        no_helmets.append(head)
                else:
                    no_helmets.append(head)

                helmet_index += 1

            image_preview = frame.copy()
            coco_bbox = []

            for head in helmets:
                coco_bbox.append({ "category": head.category, "bbox": head.coco(args.helmets_padding)})

                ymin, ymax, xmin, xmax = head.bbox(args.helmets_padding)
                
                # BBox draw
                cv2.rectangle(
                    image_preview,
                    (xmin, ymin),
                    (xmax, ymax),
                    color=(0, 0, 255),
                    thickness=2
                )
            
            for head in no_helmets:
                coco_bbox.append({ "category": head.category, "bbox": head.coco(args.helmets_padding, False)})

                ymin, ymax, xmin, xmax = head.bbox(args.no_helmets_padding, False)
                # BBox draw
                cv2.rectangle(
                    image_preview,
                    (xmin, ymin),
                    (xmax, ymax),
                    color=(255, 0, 0),
                    thickness=2
                )

            out_name = output_path + input_name + '_' + str(frame_index) + '_' + str(timestamp)

            cv2.imwrite(out_name + '.jpg', frame)
            cv2.imwrite(out_name + '_mask.png', frame_mask)

            (height, width) = frame.shape[:2]
            save_yolo_txt(out_name  + '.txt', height, width, coco_bbox)

            img = cv2.cvtColor(image_preview, cv2.COLOR_RGB2BGR)
            plt.figure(figsize=(10,6))
            plt.imshow(img)
            plt.axis("off")
            plt.show()

            data_index += 1
    else:
        print("No data for estimated heads position")

    if cap:
        cap.release()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        type=str,
        default='',
        help='Path of the mp4 input file',
    )
    parser.add_argument(
        '--output',
        type=str,
        default='',
        help='Path of synthesized output images and labels',
    )
    parser.add_argument(
        '--mask_or_nomask',
        type=str,
        default='mask',
        choices=[
            'mask',
            'nomask',
        ],
        help='\
            Select either a model that provides high accuracy when wearing \
            a mask or a model that provides high accuracy when not wearing a mask',
    )
    parser.add_argument(
        '--helmets',
        type=str,
        default='',
        help='\
            Path of the helmets png images files directories splitted in: \
            front, front_left, front_right, rear, rear_left, rear_right, left and right',
    )
    parser.add_argument(
        '--frames',
        type=int,
        default=0,
        help='Count of frames to parse from start of video. Default: None (All)'
    )
    parser.add_argument(
        '--count',
        type=str,
        default=None,
        help='Count of synthetized images or list of frames splitted with comma (Ex. 0,1,3). Default: None (All)'
    )
    parser.add_argument(
        '--percent',
        type=float,
        default=50,
        help='Synthesized helmets percent per frame. Default: 50'
    )
    parser.add_argument(
        '--scale',
        type=float,
        default=15,
        help='Head bounding box scale percent for helmets. Default: 15'
    )
    parser.add_argument(
        '--min_size',
        type=int,
        default=None,
        help='Minimum head bbox height or width for helmets. Default: None'
    )
    parser.add_argument(
        '--max_size',
        type=int,
        default=None,
        help='Maximum head bbox height or width for helmets. Default: None'
    )
    parser.add_argument(
        '--helmets_padding',
        type=float,
        default=0,
        help='Helmets bounding box padding percent. Default: 0'
    )
    parser.add_argument(
        '--no_helmets_padding',
        type=float,
        default=0,
        help='No helmets bounding box padding percent: Default: 0'
    )
    parser.add_argument(
        '--ignore',
        type=str,
        default=None,
        help='List of head yaw types to ignore, splitted by comma. Default: None'
    )
    parser.add_argument(
        '--cache',
        type=str,
        default="True",
        help='Use estimated heads position cached json file.',
    )
    parser.add_argument(
        '--debug',
        type=str,
        default="False",
        help='Show debug information. Default: False'
    )
    args = parser.parse_args()
    main(args)