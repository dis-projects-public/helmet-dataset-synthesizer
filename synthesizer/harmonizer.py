import argparse
import os
from pathlib import Path
import sys

import cv2
import numpy as np
import torch
from tqdm import tqdm
import yaml
from easydict import EasyDict as edict
from typing import Optional

sys.path.append('./cdtnet')
from iharm.inference.predictor import Predictor
from iharm.inference.utils import load_model

class Harmonizer(object):
    def __init__(
        self,
        model_type: Optional[str] = 'CDTNet',
        checkpoint_path: Optional[str] = './cdtnet/HAdobe5k_2048.pth'
    ):
        device = torch.device(f'cuda:0') if torch.cuda.is_available() else "cpu"
        net = load_model(model_type, checkpoint_path, verbose=True)
        self.predictor = Predictor(net, device)

    def __call__(self, image, mask):
        rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_size = rgb_image.shape

        mask_image = mask[:, :, 0]
        mask_image[mask_image <= 100] = 0
        mask_image[mask_image > 100] = 1
        mask_image = mask_image.astype(np.float32)

        pred = self.predictor.predict(rgb_image, mask_image)

        pred = cv2.resize(pred, image_size[:-1][::-1])

        return cv2.cvtColor(pred, cv2.COLOR_RGB2BGR)