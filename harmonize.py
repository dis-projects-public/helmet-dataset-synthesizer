import argparse
import os
from pathlib import Path
import sys

import cv2
import numpy as np
import torch
from tqdm import tqdm
import yaml
from easydict import EasyDict as edict

sys.path.append('./cdtnet')
from iharm.inference.predictor import Predictor
from iharm.inference.utils import load_model, find_checkpoint
from iharm.mconfigs import ALL_MCONFIGS
from iharm.utils.log import logger

def load_config_file(config_path, model_name=None, return_edict=False):
    with open(config_path, 'r') as f:
        cfg = yaml.safe_load(f)

    if 'SUBCONFIGS' in cfg:
        if model_name is not None and model_name in cfg['SUBCONFIGS']:
            cfg.update(cfg['SUBCONFIGS'][model_name])
        del cfg['SUBCONFIGS']

    return edict(cfg) if return_edict else cfg

def main():
    args, cfg = parse_args()

    device = torch.device(f'cuda:{args.gpu}')
    checkpoint_path = find_checkpoint(cfg.MODELS_PATH, args.checkpoint)
    print(checkpoint_path)
    net = load_model(args.model_type, checkpoint_path, verbose=True)
    #model, _ = net.init_model(cfg)
    predictor = Predictor(net, device)

    def _save_image(image_name, bgr_image):
        rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_RGB2BGR)
        cv2.imwrite(
            str(cfg.RESULTS_PATH / f'{image_name}'),
            rgb_image,
            #[cv2.IMWRITE_JPEG_QUALITY, 85]
        )

    logger.info(f'Save images to {cfg.RESULTS_PATH}')

    input_path = os.path.dirname(args.input)
    image_names = []

    image_name, image_extension = os.path.splitext(os.path.basename(args.input))
    if image_extension:
        if image_extension != ".jpg" :
            logger.error('Invalid .jpg image file')
            sys.exit(1)

        image_names.append(image_name + image_extension)
    else:
        image_names = filter(lambda x: x.endswith(('.jpg')), os.listdir(args.input))

    if not len(image_names):
        logger.error('Not input .jpg images found')
        sys.exit(1)
    else:
        print('Input:', image_names)

    masks_path = args.masks if args.masks else input_path

    print('Harmonizing...')

    for image_name in tqdm(image_names):
        image_path = os.path.join(input_path, image_name)

        if not os.path.exists(image_path):
            logger.error(f'Image not found in {image_path}')
            continue

        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_size = image.shape

        mask_name = os.path.splitext(os.path.basename(image_name))[0]
        mask_path = os.path.join(masks_path, f'{mask_name}{args.mask_postfix}.png')

        if not os.path.exists(mask_path):
            logger.error(f'Mask image not found in {mask_path}')
            continue

        mask_image = cv2.imread(mask_path)
        mask = mask_image[:, :, 0]
        mask[mask <= 100] = 0
        mask[mask > 100] = 1
        mask = mask.astype(np.float32)

        pred = predictor.predict(image, mask)

        pred = cv2.resize(pred, image_size[:-1][::-1])
        _save_image(image_name, pred)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_type', choices=ALL_MCONFIGS.keys())
    parser.add_argument('--checkpoint', type=str,
                        default='./cdtnet/HAdobe5k_2048.pth',
                        help='The path to the checkpoint. '
                             'This can be a relative path (relative to cfg.MODELS_PATH) '
                             'or an absolute path. The file extension can be omitted')
    parser.add_argument(
        '--input', type=str,
        help='Path to .jpg image file or directory with .jpg images to harmonize'
    )
    parser.add_argument(
        '--output', type=str, default='',
        help='The path to the harmonized images. Default path: cfg.EXPS_PATH/predictions'
    )
    parser.add_argument(
        '--mask_postfix', type=str,
        default='',
        help='Postfix of .png masks images files, e.g. _mask'
    )
    parser.add_argument(
        '--masks', type=str,
        help='Path to directory with .png binary masks for images, named exactly like images plus --mask-postfix. If not specified, the path of --input is used'
    )
    parser.add_argument('--gpu', type=str, default=0, help='ID of used GPU.')
    parser.add_argument('--config-path', type=str, default='./cdtnet/config.yml', help='The path to the config file')

    args = parser.parse_args()
    cfg = load_config_file(args.config_path, return_edict=True)
    cfg.EXPS_PATH = Path(cfg.EXPS_PATH)
    cfg.RESULTS_PATH = Path(args.output + "/harmonized") if len(args.output) else Path(os.path.abspath(args.input) + "/harmonized")
    cfg.RESULTS_PATH.mkdir(parents=True, exist_ok=True)
    logger.info(cfg)
    return args, cfg

if __name__ == '__main__':
    main()