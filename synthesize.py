import cv2
import math
import copy
import argparse
import json
from tqdm import tqdm
import random
import matplotlib.pyplot as plt
import os
import sys
from synthesizer.helmets import SynthesizedImage

def main(args):
    if not os.path.exists(args.input):
        print("*** Input media file not exists ***")
        sys.exit(1)
   
    input_file = os.path.basename(args.input)
    input_name, input_extension = os.path.splitext(input_file)

    is_video_input = input_extension in [".mp4"]
    is_image_input = input_extension in [".jpg", ".jpeg", ".png"]
    
    if not (is_video_input or is_image_input):
        print("*** Invalid input media file ***")
        print("Input must be an (.jpg, .jpeg, .png, .mp4) media file.")
        sys.exit(1)

    output_name = f"{args.output}/{input_name}" if args.output else input_name

    print('Reading input file:', args.input)

    input_json = args.json
    _, json_extension = os.path.splitext(os.path.basename(input_json))
    if json_extension:
        if json_extension != ".json" :
            print("*** Invalid input json file ***")
            sys.exit(1)
    else:
        input_json = f"{args.json}/{input_name}.json" if args.json else f"{input_name}.json"

    if not os.path.exists(input_json):
        print("*** Input json file not exists ***")
        sys.exit(1)

    print('Reading json file:', input_json)

    with open(input_json, 'r') as openfile:
        json_object = json.load(openfile)

    data_length = len(json_object)

    if data_length == 0:
        print("*** Has not heads to synthesize ***")
        sys.exit(1)    

    if args.output:
        if not os.path.isdir(args.output):
            os.mkdir(args.output)

    # Synthesizing
    images = []

    print('Synthesizing...')
    if is_video_input:
        cap = cv2.VideoCapture(args.input)

        if args.frames:
            frames_list = [int(i) for i in args.frames.split(',')]
        elif args.count and (args.count <= data_length):
            frames_list = random.sample(range(data_length), args.count)
        else:
            frames_list = random.sample(range(data_length), random.randint(1, data_length))
    else:
        frames_list = range(data_length)

    pbar = tqdm(total=len(frames_list))

    data_index = 0
    while data_index < data_length:
        frame_data = json_object[data_index]
        
        if is_video_input:
            frame_index = frame_data["frame"]

            if not frame_index in frames_list:
                data_index += 1
                continue

            cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
            ret, frame = cap.read()
            if not ret:
                break
        else:
            frame = cv2.imread(args.input)

        synth_image = SynthesizedImage(
            scale = args.scale,
            percent = args.percent,
            min_size = args.min_size,
            max_size = args.max_size,
            helmets_padding = args.helmets_padding,
            no_helmets_padding = args.no_helmets_padding,
            pixels_padding = args.padding == "pixels",
            ignore_yaw_list = args.ignore,
            helmets_path = args.helmets
        )

        synth_image(frame, frame_data["heads"])
        images.append(synth_image)

        pbar.update(1)
        data_index += 1

    pbar.close()

    print('Saving synthesized images...')
    for synth_image in images:
        image, mask = synth_image.save_images(output_name)
        synth_image.save_labels(output_name)

        if args.preview:
            synth_image.draw_bboxes(image)

            image_preview = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            plt.figure(figsize=(10,6))
            plt.imshow(image_preview)
            plt.axis("off")
            plt.show()

            mask_preview = cv2.cvtColor(mask, cv2.COLOR_RGB2BGR)
            plt.figure(figsize=(10,6))
            plt.imshow(mask_preview)
            plt.axis("off")
            plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        type=str,
        default='',
        help='Path of the (.jpg, .jpeg, .png, .mp4) input media file. Required',
    )
    parser.add_argument(
        '--json',
        type=str,
        default='',
        help='Path of the parsed heads .json file. If file name is not specified, the .json file in path with the name of the input file is used. Required',
    )
    parser.add_argument(
        '--output',
        type=str,
        default='.',
        help='Path of the synthesized images and labels output files',
    )
    parser.add_argument(
        '--helmets',
        type=str,
        default='.',
        help='\
            Path of the helmets png images files directories splitted in: \
            front, front_left, front_right, rear, rear_left, rear_right, left and right',
    )
    parser.add_argument(
        '--frames',
        type=str,
        default='',
        nargs='?',
        help='List of frames index to synthesize from video input splitted by comma (Ex. 0,1,3). Default: None'
    )
    parser.add_argument(
        '--count',
        type=int,
        default=0,
        help='Count of frames to synthetize from video input (0 = Random count). Default: 0'
    )
    parser.add_argument(
        '--percent',
        type=float,
        default=50,
        help='Synthesized helmets percent. Default: 50'
    )
    parser.add_argument(
        '--scale',
        type=float,
        default=15,
        help='Head bounding box scale percent for helmets. Default: 15'
    )
    parser.add_argument(
        '--min_size',
        type=int,
        default=0,
        help='Minimum bbox height or width for helmets. Default: 0'
    )
    parser.add_argument(
        '--max_size',
        type=int,
        default=0,
        help='Maximum head bbox height or width for helmets. Default: 0'
    )
    parser.add_argument(
        '--helmets_padding',
        type=float,
        default=0.5,
        help='Helmets bounding box padding percent. Default: 0'
    )
    parser.add_argument(
        '--no_helmets_padding',
        type=float,
        default=0.5,
        help='No helmets bounding box padding percent: Default: 0'
    )
    parser.add_argument(
        '--padding',
        default='percent',
        choices=[
            'percent',
            'pixels'
        ],
        help='Bounding box padding style: Default: percent'
    )
    parser.add_argument(
        '--ignore',
        type=str,
        default='',
        nargs='?',
        help='\
            List of head yaw types to ignore splitted by comma in: \
            front, front_left, front_right, rear, rear_left, rear_right, left and right. Default: None',
    )
    parser.add_argument(
        '--preview',
        action='store_true',
        help='Preview synthesized images and mask'
    )
    args = parser.parse_args()
    main(args)