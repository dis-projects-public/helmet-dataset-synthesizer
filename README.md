# Helmet Dataset Synthesizer

Tools to create synthetic helmet dataset for yolo detection model.

## Install

```bash
pip install -r requirements.txt
```

## Usage

### video.py
Get estimated head position and bounding box.


```bash
python video.py \
[-h] \
[--device DEVICE] \
[--mask_or_nomask {mask,nomask}]

optional arguments:
  -h, --help
    Show this help message and exit.

  --device DEVICE
    Path of the mp4 file or device number of the USB camera.
    Default: 0

  --mask_or_nomask {mask,nomask}
    Select either a model that provides high accuracy when wearing a mask or
    a model that provides high accuracy when not wearing a mask.
    Default: mask
```

## License

## References
1. https://github.com/PINTO0309/DMHead