import os
import cv2
import time
import math
import sys
from math import cos, sin
import copy
import argparse
import numpy as np
import json
from tqdm import tqdm
from dmhead.dmhead import DMHead
import warnings

warnings.filterwarnings("ignore")

def draw_axis(img, yaw, pitch, roll, tdx=None, tdy=None, size=100):
    # Referenced from HopeNet https://github.com/natanielruiz/deep-head-pose
    if math.isnan(yaw) or math.isnan(pitch) or math.isnan(roll):
        return img
    pitch = pitch * np.pi / 180
    yaw = -(yaw * np.pi / 180)
    roll = roll * np.pi / 180
    if tdx != None and tdy != None:
        tdx = tdx
        tdy = tdy
    else:
        height, width = img.shape[:2]
        tdx = width / 2
        tdy = height / 2
    # X-Axis pointing to right. drawn in red
    x1 = size * (cos(yaw) * cos(roll)) + tdx
    y1 = size * (cos(pitch) * sin(roll) + cos(roll) * sin(pitch) * sin(yaw)) + tdy
    # Y-Axis | drawn in green
    #        v
    x2 = size * (-cos(yaw) * sin(roll)) + tdx
    y2 = size * (cos(pitch) * cos(roll) - sin(pitch) * sin(yaw) * sin(roll)) + tdy
    # Z-Axis (out of the screen) drawn in blue
    x3 = size * (sin(yaw)) + tdx
    y3 = size * (-cos(yaw) * sin(pitch)) + tdy
    cv2.line(img, (int(tdx), int(tdy)), (int(x1),int(y1)),(0,0,255),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x2),int(y2)),(0,255,0),2)
    cv2.line(img, (int(tdx), int(tdy)), (int(x3),int(y3)),(255,0,0),2)
    return img

def main(args):
    if not os.path.exists(args.input):
        print("*** Input media file not exists ***")
        sys.exit(1)
   
    input_file = os.path.basename(args.input)
    input_name, input_extension = os.path.splitext(input_file)

    output_name = f"{args.output}/{input_name}" if args.output else input_name
    output_file = f"{output_name}_heads{input_extension}"
    output_json = f"{output_name}.json"

    is_video_input = input_extension in [".mp4"]
    is_image_input = input_extension in [".jpg", ".jpeg", ".png"]
    
    if not (is_video_input or is_image_input):
        print("*** Invalid input media file ***")
        print("Input must be an (.jpg, .jpeg, .png, .mp4) media file.")
        sys.exit(1)

    print('Reading input file:', args.input)

    if args.output:
        if not os.path.isdir(args.output):
            os.mkdir(args.output)

    # DMHead
    print('Loading models...')
    if  args.mask_or_nomask == 'mask':
        model_file_path = 'dmhead_mask_Nx3x224x224.onnx'
    else:
        model_file_path = 'dmhead_nomask_Nx3x224x224.onnx'

    dmhead = DMHead(dmhead_model_path = model_file_path)

    # Parsing heads
    print('Parsing...')
    if is_video_input:
        cap = cv2.VideoCapture(args.input)
        cap_fps = cap.get(cv2.CAP_PROP_FPS)
        cap_frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(
            filename=output_file,
            fourcc=fourcc,
            fps=cap_fps,
            frameSize=(w, h),
        )

        frame_count = args.frames if args.frames else cap_frame_count
    else:
        frame_count = 1

    pbar = tqdm(total=frame_count)

    frame_index = 0
    output_data = []

    while True:
        if is_video_input:
            ret, frame = cap.read()
            if not ret:
                break
        else:
            frame = cv2.imread(args.input)

        start = time.time()

        # Inference DMHead
        heads, poses = dmhead(frame)

        # Initialize result
        heads_data = []
        canvas = copy.deepcopy(frame)

        if heads.size > 0:
            # Parse heads and poses
            for (yaw, roll, pitch), position in zip(poses, heads):
                yaw, pitch, roll = np.squeeze([yaw, pitch, roll])
                #print(f'yaw: {yaw}, pitch: {pitch}, roll: {roll}')

                x_min, y_min, x_max, y_max = position

                # Heads data
                heads_data.append({
                    "x": int(x_min),
                    "y": int(y_min),
                    "width": int(x_max - x_min),
                    "height": int(y_max - y_min),
                    "yaw": int(yaw),
                    "pitch": int(pitch),
                    "roll": int(roll)
                })

                # BBox draw
                deg_norm = 1.0 - abs(yaw / 180)
                blue = int(255 * deg_norm)
                cv2.rectangle(
                    canvas,
                    (int(x_min), int(y_min)),
                    (int(x_max), int(y_max)),
                    color=(blue, 0, 255-blue),
                    thickness=2
                )

                # Draw axis
                draw_axis(
                    canvas,
                    yaw,
                    pitch,
                    roll,
                    tdx=(x_min+x_max)/2,
                    tdy=(y_min+y_max)/2,
                    size=abs(x_max-x_min)//2
                )
                cv2.putText(
                    canvas,
                    f'yaw: {np.round(yaw)}',
                    (int(x_min), int(y_min)),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.4,
                    (100, 255, 0),
                    1
                )
                cv2.putText(
                    canvas,
                    f'pitch: {np.round(pitch)}',
                    (int(x_min), int(y_min) - 15),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.4,
                    (100, 255, 0),
                    1
                )
                cv2.putText(
                    canvas,
                    f'roll: {np.round(roll)}',
                    (int(x_min), int(y_min)-30),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.4,
                    (100, 255, 0),
                    1
                )

            output_data.append({"frame": frame_index, "heads": heads_data})

        time_txt = f'Inference: {(time.time()-start)*1000:.2f} ms'
        info_txt = f'Frame: {frame_index} - {time_txt}' if is_video_input else time_txt

        cv2.putText(
            canvas,
            info_txt,
            (20, 35),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.8,
            (255, 255, 255),
            2,
            cv2.LINE_AA,
        )
        cv2.putText(
            canvas,
            info_txt,
            (20, 35),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.8,
            (0, 255, 0),
            1,
            cv2.LINE_AA,
        )

        if is_video_input:
            video_writer.write(canvas)
        else:
            cv2.imwrite(output_file, canvas)
            
        pbar.update(1)
        frame_index += 1

        if frame_index == frame_count:
            break
    
    pbar.close()

    print('Writing output file:', output_file)
    print('Writing output data:', output_json)

    # Save predicted heads in json file
    json_object = json.dumps(output_data, indent=4)
    with open(output_json, "w") as outfile:
        outfile.write(json_object)

    if is_video_input:
        print('* Frames with heads:', len(output_data))

        video_writer.release()
        cap.release()
    else:
        print('* Predicted heads:', heads.size)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        type=str,
        default='',
        help='Path of the (.jpg, .jpeg, .png, .mp4) input media file.',
    )
    parser.add_argument(
        '--output',
        type=str,
        default='',
        help='Path of the media and json output file.',
    )
    parser.add_argument(
        '--mask_or_nomask',
        type=str,
        default='mask',
        choices=[
            'mask',
            'nomask',
        ],
        help='\
            Select either a model that provides high accuracy when wearing \
            a mask or a model that provides high accuracy when not wearing a mask.',
    )
    parser.add_argument(
        '--frames',
        type=int,
        default=None,
        help='Count of frames to parse from start of video.'
    )
    args = parser.parse_args()
    main(args)