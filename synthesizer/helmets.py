import cv2
import copy
import numpy as np
from typing import Tuple, Optional, List
import random
import base64
import glob
import os
import json
from enum import Enum

class YAW(Enum):
    FRONT = "front"
    FRONT_LEFT = "front_left"
    FRONT_RIGHT = "front_right"
    REAR = "rear"
    REAR_LEFT = "rear_left"
    REAR_RIGHT = "rear_right"
    LEFT = "left"
    RIGHT = "right"

    def __str__(self):
        return self.value

    @classmethod
    def from_value(cls, value):
        if value >= 0:
            if value >= 150:
                return cls.REAR
            elif value >= 120:
                return cls.REAR_LEFT
            elif value >= 60:
                return cls.LEFT
            elif value >= 30:
                return cls.FRONT_LEFT
        else:
            if value <= -150:
                return cls.REAR
            elif value <= -120:
                return cls.REAR_RIGHT
            elif value <= -60:
                return cls.RIGHT
            elif value <= -30:
                return cls.FRONT_RIGHT

        return cls.FRONT

class HEAD(object):
    def __init__(self, bbox, scale=None):
        self.x_min = int(bbox['x'])
        self.y_min = int(bbox['y'])
        self.width = int(bbox['width'])
        self.height = int(bbox['height'])
        self.x_max = self.x_min + self.width
        self.y_max = self.y_min + self.height
        self.roll = bbox["roll"]
        self.pitch = bbox["pitch"]
        self.yaw = YAW.from_value(bbox["yaw"])

        if scale:
            self.resize(scale)

    def overlap(self, head):
        if self.x_min > head.x_max or head.x_min > self.x_max:
            return False
    
        # If one rectangle is above other
        if self.y_min > head.y_max or head.y_min > self.y_max:
            return False
    
        return True

    def area(self):
        return (self.x_max - self.x_min) * (self.y_max - self.y_min)

    def resize(self, scale):
        # Width
        w_diff = int(scale * self.width) // 2
        self.x_min -= w_diff
        self.x_max += w_diff
        # Height
        h_diff = int(scale * self.height) // 2
        self.y_min -= h_diff
        self.y_max += h_diff

        self.width = self.x_max - self.x_min
        self.height = self.y_max - self.y_min

    def set_padding(self, padding):
        if padding < 1:
            self.resize(padding)
        else:
            int_pad = int(padding)
            # Width
            self.x_min -= int_pad
            self.x_max += int_pad
            # Height
            self.y_min -= int_pad
            self.y_max += int_pad

            self.width = self.x_max - self.x_min
            self.height = self.y_max - self.y_min

    def to_bbox(self):
        return {'x': self.x_min, 'y': self.y_min, 'width': self.width, 'height':  self.height}

class HELMET(object):
    def __init__(self, bbox, label, padding, image=None):
        if isinstance(bbox, HEAD):
            self.x = bbox.x_min
            self.y = bbox.y_min
            self.width = bbox.width
            self.height = bbox.height
        else:
            self.x = int(bbox['x'])
            self.y = int(bbox['y'])
            self.width = int(bbox['width'])
            self.height = int(bbox['height'])

        self.label = label
        self.image = image
        self.padding = padding
        self.image_size = None

        if not image is None:
            self.update_image_size()
            self.strech_bbox()
        elif padding:
            self.set_padding(padding)

    def resize(self, scale):
        # Width
        w_diff = int(scale * self.width) // 2
        self.x -= w_diff
        self.width += w_diff * 2
        # Height
        h_diff = int(scale * self.height) // 2
        self.y -= h_diff
        self.height += h_diff * 2

    def set_padding(self, padding):
        if padding < 1:
            self.resize(padding)
        else:
            int_pad = int(padding)
            # Width
            self.x -= int_pad
            self.width += int_pad * 2
            # Height
            self.y -= int_pad
            self.height += int_pad * 2

    def update(self, bbox):
        if isinstance(bbox, HEAD):
            self.x = bbox.x_min
            self.y = bbox.y_min
            self.width = bbox.width
            self.height = bbox.height
        else:
            self.x = int(bbox['x'])
            self.y = int(bbox['y'])
            self.height = int(bbox['height'])
            self.width = int(bbox['width'])

        if not self.image is None:
            self.update_image_size(True)

        return self

    def need_image(self, min_size):
        if (self.label != "no_helmet") and (self.image is None):
            return (self.width > min_size) and (self.height > min_size)
        else:
            return False

    def update_image(self, image):
        if not image is None:
            self.image = image
            self.update_image_size(True)

    def update_image_size(self, with_padding=False):
        if with_padding and self.padding:
            width = self.width - int(self.width * self.padding) if self.padding < 1 else width - int(self.padding)
            height = self.height - int(self.height * self.padding) if self.padding < 1 else height - int(self.padding)
        else:
            width = self.width
            height = self.height

        (h, w) = self.image.shape[:2]

        scale_w = float(width)/float(w)
        scale_h = float(height)/float(h)
        if scale_h > scale_w:
            self.image_size = (int(w * scale_w), int(h * scale_w))
        else:
            self.image_size = (int(w * scale_h), int(h * scale_h))

        """
        if width > height:
            r = width / float(w)
            self.image_size = (width, int(h * r))
        else:
            r = height / float(h)
            self.image_size = (int(w * r), height)
        """

    def get_resized_image(self):
        image = self.image.copy()

        return cv2.resize(image, self.image_size, interpolation = cv2.INTER_AREA)

    def strech_bbox(self):
        (w, h) = self.image_size

        x_center = int(self.width / 2) + self.x
        y_center = int(self.height / 2) + self.y
        self.x = x_center - int(w / 2)
        self.y = y_center - int(h / 2)
        self.width = w
        self.height = h

        if self.padding:
            self.set_padding(self.padding)

    def draw_helmet(self, background, background_mask=None):
        if self.image is None:
            return

        image = self.get_resized_image()

        bg_h, bg_w, bg_channels = background.shape
        fg_h, fg_w, fg_channels = image.shape

        assert bg_channels == 3, f'background image should have exactly 3 channels (RGB). found:{bg_channels}'
        assert fg_channels == 4, f'foreground image should have exactly 4 channels (RGBA). found:{fg_channels}'

        # Calcule offset
        (fh, fw) = image.shape[:2]

        x_center = int(self.width / 2) + self.x
        y_center = int(self.height / 2) + self.y
        x_offset = x_center - int(fw / 2)
        y_offset = y_center - int(fh / 2)

        w = min(fg_w, bg_w, fg_w + x_offset, bg_w - x_offset)
        h = min(fg_h, bg_h, fg_h + y_offset, bg_h - y_offset)

        if w < 1 or h < 1: return

        # clip foreground and background images to the overlapping regions
        bg_x = max(0, x_offset)
        bg_y = max(0, y_offset)
        fg_x = max(0, x_offset * -1)
        fg_y = max(0, y_offset * -1)
        image = image[fg_y:fg_y + h, fg_x:fg_x + w]
        background_subsection = background[bg_y:bg_y + h, bg_x:bg_x + w]

        # separate alpha and color channels from the foreground image
        foreground_colors = image[:, :, :3]
        alpha_channel = image[:, :, 3] / 255  # 0-255 => 0.0-1.0

        # construct an alpha_mask that matches the image shape
        alpha_mask = np.dstack((alpha_channel, alpha_channel, alpha_channel))

        # combine the background with the overlay image weighted by alpha
        composite = background_subsection * (1 - alpha_mask) + foreground_colors * alpha_mask

        # overwrite the section of the background image that has been updated
        background[bg_y:bg_y + h, bg_x:bg_x + w] = composite

        if background_mask is None:
            return
        else:
            mask_subsection = background_mask[bg_y:bg_y + h, bg_x:bg_x + w]
            mask = np.zeros(image.shape, dtype=np.uint8)
            mask.fill(255)
            background_mask[bg_y:bg_y + h, bg_x:bg_x + w] = mask_subsection * (1 - alpha_mask) + mask[:, :, :3] * alpha_mask

    def draw_bbox(self, background):
        cv2.rectangle(
            background,
            (self.x, self.y),
            (self.x + self.width, self.y + self.height),
            color=(255, 0, 0) if self.label == "no_helmet" else (0, 0, 255),
            thickness=2
        )

    def to_bbox(self):
        return {'x': self.x, 'y': self.y, 'width': self.width, 'height':  self.height, 'label': self.label}

    def to_yolo(self, height, width):
        # YOLO bounding box format: [x_center, y_center, width, height]
        # (float values relative to width and height of image)

        dw = 1.0 / width
        dh = 1.0 / height

        x_center = self.x + self.width / 2.0
        y_center = self.y + self.height / 2.0

        x = x_center * dw
        y = y_center * dh
        w = self.width * dw
        h = self.height * dh

        return [x, y, w, h]

class SynthesizedImage(object):
    def __init__(
        self,
        scale: Optional[float] = 30,
        percent: Optional[float] = 50,
        overlap: Optional[float] = 10,
        min_size: Optional[int] = 0,
        max_size: Optional[int] = 0,
        helmets_padding: Optional[float] = 7,
        no_helmets_padding: Optional[float] = 7,
        helmets_category: Optional[int] = 0,
        no_helmets_category: Optional[int] = 1,
        pixels_padding: Optional[bool] = False,
        ignore_yaw_list: Optional[str] = '',
        helmets_path: Optional[str] = '.'
    ):
        self.scale = scale / 100
        self.percent = percent / 100
        self.overlap = 1 + (overlap / 100)
        self.min_size = min_size
        self.max_size = max_size
        self.helmets_padding = helmets_padding if pixels_padding else helmets_padding / 100
        self.no_helmets_padding = no_helmets_padding if pixels_padding else no_helmets_padding / 100
        self.helmets_category = helmets_category
        self.no_helmets_category = no_helmets_category
        self.ignore_yaw_list = ignore_yaw_list.split(',') if ignore_yaw_list else []

        self.helmets_path = helmets_path
        self.helmets_dirs = self.list_helmets_dirs(helmets_path)

        self.helmets = []
        self.image = None

    def parse_heads(self, bboxes):
        helmets = []
        no_helmets = []

        (h, w) = self.image.shape[:2]

        for a in bboxes:
            success = True
            head1 = HEAD(a, self.scale)

            for b in bboxes:
                if a != b:
                    head2 = HEAD(b, self.scale)
                    if head1.overlap(head2):
                        if head1.area() < head2.area() * self.overlap:
                            success = False
                            break

                    if self.min_size:
                        if head1.width < self.min_size:
                            success = False
                            break

                        if head1.height < self.min_size:
                            success = False
                            break

                    if self.max_size:
                        if head1.width > self.max_size:
                            success = False
                            break

                        if head1.height > self.max_size:
                            success = False
                            break

            # Check if yaw in ignore list
            if success and len(self.ignore_yaw_list):
                if str(head1.yaw) in self.ignore_yaw_list:
                    success = False

            # Check if bbox size exceeds image size with padding
            if success:
                head1.set_padding(self.helmets_padding)
                if (head1.x_max > w) or (head1.y_max > h):
                    success = False

            if success:
                helmets.append(HEAD(a))
            else:
                no_helmets.append(HEAD(a))

        return helmets, no_helmets

    def list_helmets_dirs(self, path):
        directories = [x[1] for x in os.walk(path)]
        non_empty_dirs = [x for x in directories if x]
        return [item for subitem in non_empty_dirs for item in subitem]

    def get_helmet_path(self, yaw):
        if yaw == YAW.REAR:
            return "rear", False if "rear" in self.helmets_dirs else (None, False)
        elif yaw == YAW.REAR_LEFT:
            return ("rear_left", False) if "rear_left" in self.helmets_dirs else ("rear_right", True) if "rear_right" in self.helmets_dirs else (None, False)
        elif yaw == YAW.LEFT:
            return ("left", False) if "left" in self.helmets_dirs else ("right", True) if "right" in self.helmets_dirs else (None, False)
        elif yaw == YAW.FRONT_LEFT:
            return ("front_left", False) if "front_left" in self.helmets_dirs else ("front_right", True) if "front_right" in self.helmets_dirs else (None, False)
        elif yaw == YAW.REAR_RIGHT:
            return ("rear_right", False) if "rear_right" in self.helmets_dirs else ("rear_left", True) if "rear_left" in self.helmets_dirs else (None, False)
        elif yaw == YAW.RIGHT:
            return ("right", False) if "right" in self.helmets_dirs else ("left", True) if "left" in self.helmets_dirs else (None, False)
        elif yaw == YAW.FRONT_RIGHT:
            return ("front_right", False) if "front_right" in self.helmets_dirs else ("front_left", True) if "front_left" in self.helmets_dirs else (None, False)

        return "front", False if "front" in self.helmets_dirs else (None, False)

    def get_helmet_image(self, yaw):
        path, flip = self.get_helmet_path(yaw)

        if path:
            images = glob.glob(self.helmets_path + '/' + path + '/*.png')

            if len(images):
                index = random.randint(0, len(images)-1)
                image = cv2.imread(images[index], cv2.IMREAD_UNCHANGED)

                if flip:
                    image = cv2.flip(image, 1)

                return image

        return None

    def get_sythesized_image(self, as_base64: Optional[bool] = False, with_mask: Optional[bool] = False):
        image = self.image.copy()

        if with_mask:
            image_mask = self.image.copy()
            image_mask[:] = (0, 0, 0)
        else:
            image_mask = None

        for helmet in self.helmets:
           helmet.draw_helmet(image, image_mask)

        if as_base64:
            success, im_buf_arr = cv2.imencode(".jpg", image)
            encoded = str(base64.b64encode(im_buf_arr.tobytes()), 'utf-8')

            return "data:image/jpg;base64," + encoded
        else:
            return image, image_mask

    def get_bboxes(self):
        bboxes = []
        for helmet in self.helmets:
            bboxes.append(helmet.to_bbox())

        return bboxes

    def strech_bboxes(self):
        for helmet in self.helmets:
            if helmet.label != "no_helmet":
                helmet.strech_bbox()

    def add(self, bbox, label):
        self.helmets.append(HELMET(bbox, label, self.no_helmets_padding if label == "no_helmet" else self.helmets_padding))

    def remove(self, index):
        if index < len(self.helmets):
            del self.helmets[index]

    def update(self, index, bbox):
        if index < len(self.helmets):
            helmet = self.helmets[index].update(bbox)
            if helmet.need_image(self.min_size if self.min_size else 10):
                helmet.update_image(self.get_helmet_image(YAW(helmet.label)))

            return helmet.image_size, (helmet.width, helmet.height)

        return None, None

    def update_all(self, bboxes):
        index = 0
        for bbox in bboxes:
            if index < len(self.helmets):
                if bbox != self.helmets[index]:
                    self.update(index, bbox)
            index += 1

    def save_images(self, filename):
        image, mask = self.get_sythesized_image(with_mask=True)

        cv2.imwrite(filename + '.jpg', image)
        cv2.imwrite(filename + '_mask.png', mask)

        return image, mask

    def save_labels(self, filename):
        (height, width) = self.image.shape[:2]

        with open(filename + '.txt', "w") as f:
            for helmet in self.helmets:
                category = self.no_helmets_category if helmet.label == "no_helmet" else self.helmets_category
                x, y, w, h = helmet.to_yolo(height, width)
                f.write(f"{category} {x:.6f} {y:.6f} {w:.6f} {h:.6f}\n")

    def draw_bboxes(self, background):
        for helmet in self.helmets:
            helmet.draw_bbox(background)

    def __call__(
        self,
        image: np.ndarray,
        bboxes: List
    ):
        self.image = image.copy()
        self.helmets = []
        try_helmets, no_helmets = self.parse_heads(bboxes)

        count = int(len(bboxes) * self.percent)
        helmets_count = len(try_helmets)
        synthesized = list(range(helmets_count)) if count >= helmets_count else random.sample(range(helmets_count), count)
        
        index = 0
        for head in try_helmets:
            if index in synthesized:
                helmet_image = self.get_helmet_image(head.yaw)
                if helmet_image is None:
                    no_helmets.append(head)
                else:
                    head.resize(self.scale)
                    self.helmets.append(HELMET(head, str(head.yaw), self.helmets_padding, helmet_image))
            else:
                no_helmets.append(head)

            index += 1

        for head in no_helmets:
            self.helmets.append(HELMET(head, "no_helmet", self.no_helmets_padding))

        return self
        
"""
class HelmetSynthesizer(object):
    def __init__(
        self,
        scale: Optional[float] = 30,
        percent: Optional[float] = 50,
        overlap: Optional[float] = 10,
        min_size: Optional[int] = 0,
        max_size: Optional[int] = 0,
        padding_type: Optional[str] = 'percent',
        helmets_padding: Optional[float] = 7,
        no_helmets_padding: Optional[float] = 7,
        ignore_yaw_list: Optional[str] = '',
        helmets_path: Optional[str] = '.',
        images_count: Optional[int] = 0,
        frames_list: Optional[str] = ''
    ):
        self.scale = scale
        self.percent = percent
        self.overlap = overlap
        self.min_size = min_size
        self.max_size = max_size
        self.helmets_padding = helmets_padding
        self.no_helmets_padding = no_helmets_padding
        self.pixels_padding = padding_type == "pixels"
        self.ignore_yaw_list = ignore_yaw_list
        self.helmets_path = helmets_path
        self.images_count = images_count
        self.frames_list = frames_list

    def __call__(self, input_path, json_path):
        if not os.path.exists(input_path):
            print("*** Media file not exists ***")
            return None
   
        input_file = os.path.basename(input_path)
        input_name, input_extension = os.path.splitext(input_file)

        is_video_input = input_extension in [".mp4"]
        is_image_input = input_extension in [".jpg", ".jpeg", ".png"]
    
        if not (is_video_input or is_image_input):
            print("*** Invalid media file ***")
            print("Input must be an (.jpg, .jpeg, .png, .mp4) media file.")
            return None

        json_file = json_path
        _, json_extension = os.path.splitext(os.path.basename(json_file))
        if json_extension:
            if json_extension != ".json" :
                print("*** Invalid json file ***")
                return None
        else:
            json_file = f"{json_path}/{input_name}.json" if json_path else f"{input_name}.json"

        if not os.path.exists(json_file):
            print("*** Json file not exists ***")
            return None

        with open(json_file, 'r') as openfile:
            json_object = json.load(openfile)

        if  len(json_object) == 0:
            print("*** Has not heads to synthesize ***")
            return None

        # Synthesizing
        images = []

        data_length = len(json_object)

        if is_video_input:
            cap = cv2.VideoCapture(input_path)

            if self.frames_list:
                frames_list = [int(i) for i in self.frames_list.split(',')]
            elif  self.images_count and ( self.images_count <= data_length):
                frames_list = random.sample(range(data_length),  self.images_count)
            else:
                frames_list = random.sample(range(data_length), random.randint(1, data_length))
        else:
            frames_list = range(data_length)

        data_index = 0
        while data_index < data_length:
            frame_data = json_object[data_index]
            
            if is_video_input:
                frame_index = frame_data["frame"]

                if not frame_index in frames_list:
                    data_index += 1
                    continue

                cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
                ret, frame = cap.read()
                if not ret:
                    break
            else:
                frame = cv2.imread(input_path)

            synth_image = SynthesizedImage(
                scale = self.scale,
                percent = self.percent,
                min_size = self.min_size,
                max_size = self.max_size,
                pixels_padding = self.pixels_padding,
                helmets_padding = self.helmets_padding,
                no_helmets_padding = self.no_helmets_padding,
                ignore_yaw_list = self.ignore_yaw_list,
                helmets_path = self.helmets_path
            )

            synth_image(frame, frame_data["heads"])
            images.append(synth_image)
            data_index += 1

        return images
"""

class HelmetSynthesizer(object):
    def __init__(
        self,
        scale: Optional[float] = 30,
        percent: Optional[float] = 50,
        overlap: Optional[float] = 10,
        min_size: Optional[int] = 0,
        max_size: Optional[int] = 0,
        padding_type: Optional[str] = 'percent',
        helmets_padding: Optional[float] = 7,
        no_helmets_padding: Optional[float] = 7,
        helmets_category: Optional[int] = 0,
        no_helmets_category: Optional[int] = 1,
        ignore_yaw_list: Optional[str] = '',
        helmets_path: Optional[str] = '.',
        input_path: Optional[str] = '',
        json_path: Optional[str] = ''
    ):
        self.cap = None
        self.image = None
        self.data_length = 0

        if not os.path.exists(input_path):
            print("*** Media file not exists ***")
            return
   
        input_file = os.path.basename(input_path)
        input_name, input_extension = os.path.splitext(input_file)

        is_video_input = input_extension in [".mp4"]
        is_image_input = input_extension in [".jpg", ".jpeg", ".png"]

        if not (is_video_input or is_image_input):
            print("*** Invalid media file ***")
            print("Input must be an (.jpg, .jpeg, .png, .mp4) media file.")
            return

        json_file = json_path
        _, json_extension = os.path.splitext(os.path.basename(json_file))
        if json_extension:
            if json_extension != ".json" :
                print("*** Invalid json file ***")
                return
        else:
            json_file = f"{json_path}/{input_name}.json" if json_path else f"{input_name}.json"

        if not os.path.exists(json_file):
            print("*** Json file not exists ***")
            return

        with open(json_file, 'r') as openfile:
            self.json_object = json.load(openfile)

        self.data_length = len(self.json_object)

        if self.data_length == 0:
            print("*** Has not heads to synthesize ***")
            return

        if is_video_input:
            self.cap = cv2.VideoCapture(input_path)
        else:
            self.image = cv2.imread(input_path)

        self.synth_image = SynthesizedImage(
            scale = scale,
            percent = percent,
            min_size = min_size,
            max_size = max_size,
            pixels_padding = padding_type == "pixels",
            helmets_padding = helmets_padding,
            no_helmets_padding = no_helmets_padding,
            helmets_category = helmets_category,
            no_helmets_category = no_helmets_category,
            ignore_yaw_list = ignore_yaw_list,
            helmets_path = helmets_path
        )

    def get_frame(self, frame_index):
        if self.cap:
            if frame_index:
                for frame_data in self.json_object:
                    if frame_data["frame"] == frame_index:
                        heads = frame_data["heads"]
                        break
            else:
                frame_data = self.json_object[random.randint(0, self.data_length-1)]
                frame_index = frame_data["frame"]
                heads = frame_data["heads"]
            
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
            ret, frame = self.cap.read()
            if not ret:
                print(f"Frame index: {frame_index} not found")

            return frame, heads
        else:
            frame_data = self.json_object[0]
            return self.image, frame_data["heads"]

    def __call__(self, frame_index=None):
        if self.data_length:
            frame, heads = self.get_frame(frame_index)

            if not frame is None:
                return self.synth_image(frame, heads)

        return None