import numpy as np
from typing import Tuple, Optional, List
from dmhead.models import YOLOv7ONNX, DMHeadONNX

class DMHead(object):
    def __init__(
        self,
        yolo_model_path: Optional[str] = 'yolov7_tiny_head_0.768_post_480x640.onnx',
        yolo_model_size: Optional[List] = [480, 640],
        yolo_class_score: Optional[float] = 0.20,
        dmhead_model_path: Optional[str] = 'dmhead_mask_Nx3x224x224.onnx',
        dmhead_model_size: Optional[List] = [224, 224],
        dmhead_scale_factor: Optional[List] = [10, 5]
    ):
        """DMHead

        Parameters
        ----------
        yolo_model_path: Optional[str]
            YoloV7 ONNX file path for model
        yolo_model_size: Optional[List]
            Yolo model input size [height, width]
            Default: [480, 640]
        yolo_class_score: Optional[float]
            Score threshold. Default: 0.20
        dmhead_model_path: Optional[str]
            DMHead ONNX file path for model
        dmhead_model_size: Optional[List]
            DMHead model input size [height, width]
            Default: [224, 224]
        dmhead_scale_factor: Optional[stListr]
            Box scale factor [height, width]
            Default: [10, 5]
        """

        # YoloV7 onnx Model
        self.yolo_model = YOLOv7ONNX(
            model_path = yolo_model_path,
            model_size = yolo_model_size,
            class_score_th = yolo_class_score
        )

        # DMHead onnx Model
        self.dmhead_model = DMHeadONNX(
            model_path = dmhead_model_path,
            model_size = dmhead_model_size,
            scale_factor = dmhead_scale_factor
        )

    def __call__(
        self,
        image: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """YOLOv7ONNX

        Parameters
        ----------
        image: np.ndarray
            Entire image

        Returns
        -------
        heads: np.ndarray
            Predicted head boxes: [count, y1, x1, y2, x2]
        poses: np.ndarray
            Predicted poses: [count, yaw, pitch, roll]
        """

        heads, _ = self.yolo_model(image)

        if heads.size > 0:
            poses = self.dmhead_model(image, heads)

            return heads, poses
        else:
            return heads, numpy.array([])